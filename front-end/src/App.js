import React, { Component } from 'react';
import { ApolloProvider } from "react-apollo";

import './App.css';
import { client } from "./graphql/client";
import PostList from './components/PostList/PostList';

class App extends Component {
  
  constructor () {
    super();
    this.state = {
      posts: [{
        id: 1,
        title: 'Post Title',
        content: 'Lorem ipsum...',
        author: 'Some person',
      }],
    };
  }

  render() {
    return (
      <ApolloProvider client={client}>
        <div className="App">
          <PostList></PostList>
        </div>
      </ApolloProvider>
    );
  }
}

export default App;
