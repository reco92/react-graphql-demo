import gql from "graphql-tag";

export const postSchema = `
type Post {
	id: ID!
	title: String
	content: String
	author: String
}

type Query {
	posts: [Post]
}
`
export const PostsListQuery = gql `
query PostsListQuery {
	allPosts {
		id
		title
		content
	}
}
`;