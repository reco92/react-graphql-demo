import React, { Component } from 'react';
// import './Post.css';

class Post extends Component {

  constructor(props) {
    super(props);
    this.state = {
      post: props.info,
    };
  }

  render() {
    return (
      <div>
        <div>{this.state.post.title}</div>
        <div>
          {this.state.post.content}
        </div>
        <div>{this.state.post.author}</div>
        <hr></hr>
      </div>
    )
  }
}

export default Post;
