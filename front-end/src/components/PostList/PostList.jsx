import React, { Component } from 'react';
import { graphql } from 'react-apollo';

import Post from '../Post/Post';
import { PostsListQuery } from '../../schemas/posts';
import './PostList.css';

class PostList extends Component {
	constructor(props) {
		super(props);
		let { error, loading, allPosts } = props.data;
		this.state = {
			error,
			loading,
			posts: allPosts || []
		};
	}

	componentWillReceiveProps(props) {
		let { error, loading, allPosts } = props.data;
		this.setState({
			error,
			loading,
			posts: allPosts || []
		});
	}

	render() {
		return (
			<div>
				{(this.state.loading && <p>Loading...</p>) ||
					(this.state.error && <p>Error {this.state.error.message}</p>) ||
					(this.state.posts || []).map((post) => <Post key={post.id} info={post} />)}
			</div>
		);
	}
}

export default (PostList = graphql(PostsListQuery)(PostList));
