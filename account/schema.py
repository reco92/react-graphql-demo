import graphene

from django.contrib.auth.models import User

from graphene_django.types import DjangoObjectType

from .models import Post


class PostType(DjangoObjectType):
    class Meta:
        model = Post


class Query(graphene.ObjectType):
    all_Posts = graphene.List(PostType)

    def resolve_all_Posts(self, info, **kwargs):
        return Post.objects.all()


class CreatePost(graphene.Mutation):  
    class Arguments:
        content = graphene.String()
        title = graphene.String()
        author = graphene.Int()

    post = graphene.Field(PostType)

    @staticmethod
    def mutate(root, info, content, title, author):
        author = User.objects.get(id=author)
        post = Post(content=content, title=title, author=author)
        post.save()
        return CreatePost(post=post)


class Mutation(graphene.ObjectType):  
    create_post = CreatePost.Field()

schema = graphene.Schema(query=Query, mutation=Mutation)
